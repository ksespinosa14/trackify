import React, { useState } from 'react';
import { Alert } from 'react-bootstrap';
import SocialLogin from './SocialLogin';
import { validateEmail } from '../helpers';
import Swal from 'sweetalert2';

export default function RegistrationForm({ getForm, authenticateGoogleToken }){

  const [userName, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [alertMsg, setAlertMsg] = useState('');
  const [success, setSucess] = useState(false);


  function registerUser(){

    setSucess(false);

    if(userName !== '' && email !== '' && password1 !== '' && password2 !==''){

      let checkExists = async () => {

        const userNameExists = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/username-exists`,{
                                  method: 'POST',
                                  headers: {
                                    'Content-Type': 'application/json'
                                  },
                                  body: JSON.stringify({ userName })
                                })
                                .then(res => res.json())

        const emailExists = await fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/email-exists`,{
                              method: 'POST',
                              headers: {
                                'Content-Type': 'application/json'
                              },
                              body: JSON.stringify({ email })
                            })
                            .then(res => res.json())

          return (userNameExists || emailExists);

        }

      // check for valid email format
      if(!validateEmail(email)){

        setAlertMsg('Invalid email format');
        return;

      }
      // check if passwords match
      else if(password1 !== password2){

        setAlertMsg("Password doesn't match");
        return;

      }
      else{
        Swal.fire({
          title: 'Please wait...',
          allowOutsideClick: false,
          didOpen: () => {
            Swal.showLoading()
          }
        });

        checkExists().then(res => {

          if(res) {
            Swal.close();
            setAlertMsg('Username or Email already exists.');
          }
          else{

            fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/`,{
              method: 'POST',
              headers: {
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({ 
                userName: userName,
                email: email,
                password: password1
              })
            })
            .then(res => res.json())
            .then(data => {
              Swal.close();
              setSucess(true);
              setAlertMsg('');
              setUsername('');
              setEmail('');
              setPassword1('');
              setPassword2('');
            })

          }

        });

      }
    }
    else{
      setAlertMsg('Please fill-out all fields.');
    }
  }

	return (
    <React.Fragment>
      <form className='login__create' id="login-up">
        <h1 className="login__title">Create Account</h1>
        {alertMsg
          ?
            <Alert className='mx-auto' variant='danger'>{alertMsg}</Alert>
          :
            ''
        }

        {success
          ?
            <Alert variant='success'>
              A verification link has been sent to your email account.
            </Alert>
          :
            ''
        }

        <div className="login__box">
          <i className='bx bx-user login__icon'></i>
          <input 
            type="text" 
            placeholder="Username" 
            className="login__input"
            value={userName} 
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>
        
        <div className="login__box">
          <i className='bx bx-at login__icon'></i>
          <input 
            type="text" 
            placeholder="Email" 
            className="login__input"
            value={email} 
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>

        <div className="login__box">
          <i className='bx bx-lock login__icon'></i>
          <input 
            type="password" 
            placeholder="Password" 
            className="login__input"
            value={password1} 
            onChange={(e) => setPassword1(e.target.value)}
          />
        </div>

        <div className="login__box">
          <i className='bx bx-lock login__icon'></i>
          <input 
            type="password" 
            placeholder="Confirm Password" 
            className="login__input"
            value={password2} 
            onChange={(e) => setPassword2(e.target.value)} 
          />
        </div>

        <a className="login__button" onClick={() => registerUser()}>Sign Up</a>

        <div>
          <span className="login__account">Already have an Account? </span>
          <span 
            className="login__signup" 
            id="sign-in"
            onClick={() => getForm('login')}
          >
            Sign In
          </span>
        </div>

        <SocialLogin authenticateGoogleToken={authenticateGoogleToken} />

      </form>
    </React.Fragment>
	)
}