import { useState, useEffect, useContext } from 'react';
import { Row, Col, Form, Jumbotron, Button, InputGroup } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function TransactionForm(){

	const { setSuccess } = useContext(UserContext);
	
	const [name, setName] = useState('');
	const [type, setType] = useState('blank');
	const [category, setCategory] = useState('blank');
	const [amount, setAmount] = useState('');
	const [categoryOptions, setCategoryOptions] = useState([]);

	// change the dropdown options for category based on transaction type
	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/details`, {
		    "method": "GET",
		    "headers": {
		      "Authorization": `Bearer ${localStorage.getItem('token')}`
		    }
		})
		.then(res => res.json())
		.then(data => {
			setCategoryOptions(data.categories.filter(category => category.Type === type).map(category => {
				return (
					<option key={category.Title} value={category.Title}>{category.Title}</option>
				)
			}));
		});
	}, [type]);

	function addTransaction(e){
		e.preventDefault();

		if(name === '' || type === '' || category === '' || amount === ''){
			Swal.fire({
			  	icon: 'error',
			  	title: 'Please fill out all fields!',
			  	showConfirmButton: false,
				timer: 1500
			});
			return;
		}

		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/transaction`, {
			method: "POST",
			headers: { 
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}` 
			},
			body: JSON.stringify({
				name: name,
				type: type,
				category: category,
				amount: amount
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				setName('');
				setType('blank');
				setCategory('blank');
				setAmount('');
				setSuccess(Math.random(1));
			}
			else{
				Swal.fire({
				  position: 'top-end',
				  icon: 'error',
				  title: 'Something went wrong, Please try again later.',
				  showConfirmButton: false,
				  timer: 1500
				});
			}
		});
	}

	return(
		<Jumbotron>
			<h4>New Transaction</h4>
			<hr className="my-4" />
			<Form onSubmit={e => addTransaction(e)}>
			  <Form.Group controlId="transactionName">
			    <Form.Label>Transaction Name:</Form.Label>
			    <Form.Control 
			    	type="text" 
			    	placeholder="What's this for?"
			    	value={name}
			    	onChange={(e) => setName(e.target.value)} 
			    />
			  </Form.Group>

			  <Row>
			  	<Col md={12} lg={6}>
			  		<Form.Group controlId="transactionType">
				    	<Form.Label>Transaction Type:</Form.Label>
				    	<Form.Control 
				    		as="select"
				    		value={type}
				    		onChange={(e) => setType(e.target.value)}
				    	>
					      <option value='blank'>----</option>
					      <option value='Income'>Income</option>
					      <option value='Expense'>Expense</option>
					    </Form.Control>
				  	</Form.Group>
			  	</Col>
			  	<Col md={12} lg={6}>
			  		<Form.Group controlId="transactionCategory">
				    	<Form.Label>Category:</Form.Label>
				    	<Form.Control 
				    		as="select"
				    		value={category}
				    		onChange={(e) => setCategory(e.target.value)} 
				    		disabled={type === 'blank' ? true : false}
				    	>
					      <option value='blank'>----</option>
					      {categoryOptions}
					    </Form.Control>
					</Form.Group>
			  	</Col>
			  </Row>

			  <label htmlFor="">Amount:</label>
			  <InputGroup>
			    <Form.Control 
			    	type="Number" 
			    	placeholder="PHP"
			    	value={amount}
			    	onChange={(e) => setAmount(e.target.value)}
			    />
			    <InputGroup.Append>
			      <Button variant="primary" type="submit">Submit</Button>
			    </InputGroup.Append>
			  </InputGroup>
			  
			</Form>
		</Jumbotron>
	)
}