import { useState, useEffect } from 'react';
import { Row, Col, Form, Jumbotron, Button, InputGroup, Alert } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function CategoryForm(){

	const [type, setType] = useState('blank');
	const [name, setName] = useState('');

	function addCategory(e){
		e.preventDefault();

		if(name === '' || type === ''){
			Swal.fire({
			  	icon: 'error',
			  	title: 'Please fill out all fields!',
			  	showConfirmButton: false,
				timer: 1500
			});
			return;
		}

		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/category`, {
			method: "POST",
			headers: { 
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}` 
			},
			body: JSON.stringify({
				type: type,
				name: name
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data.error === 'duplicate-category'){
				Swal.fire({
				  position: 'top-end',
				  icon: 'error',
				  title: 'Category already exist!',
				  showConfirmButton: false,
				  timer: 1500
				});
				setType('blank');
				setName('');
			}
			else if(data === true){
				Swal.fire({
				  position: 'top-end',
				  icon: 'success',
				  title: 'Category saved!',
				  showConfirmButton: false,
				  timer: 1500
				});
				setType('blank');
				setName('');
			}
			else{
				Swal.fire({
				  position: 'top-end',
				  icon: 'error',
				  title: 'Something went wrong, Please try again later.',
				  showConfirmButton: false,
				  timer: 1500
				});
			}
		});
	}


	return (
		<Jumbotron>
			<h4>Add Categories</h4>
			<hr className="my-4" />
			<Form onSubmit={(e) => addCategory(e)}>

				<Form.Group controlId="transactionType">
			    	<Form.Label>Transaction Type:</Form.Label>
			    	<Form.Control 
			    		as="select"
			    		value={type}
			    		onChange={(e) => setType(e.target.value)}
			    	>
				      <option value='blank'>----</option>
				      <option value='Income'>Income</option>
				      <option value='Expense'>Expense</option>
				    </Form.Control>
			  	</Form.Group>

			  	<label htmlFor="">Category Name:</label>
				<InputGroup>
				    <Form.Control 
				    	type="text" 
				    	placeholder="Title"
				    	value={name}
				    	onChange={(e) => setName(e.target.value)} 
				    />
				    <InputGroup.Append>
				      <Button variant="primary" type="submit">Submit</Button>
				    </InputGroup.Append>
				 </InputGroup>

			</Form>
		</Jumbotron>
	)
}