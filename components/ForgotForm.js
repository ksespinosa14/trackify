import React, { useState, useContext } from 'react';
import { useRouter } from 'next/router';
import { Alert } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { validateEmail } from '../helpers';
import moment from 'moment';



export default function ForgotForm({ getForm }){

  const { user, setUser } = useContext(UserContext);
  const Router = useRouter();

  const [email, setEmail] = useState('');
  const [alertMsg, setAlertMsg] = useState('');

  function authenticate(){

    if(email === '' || !validateEmail(email)) setAlertMsg('Please provide a valid email.');
    else{

      setAlertMsg('');
      fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/forgot`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({email})
      })
      .then(res => res.json())
      .then(data => {
        
        if(!data){
          setAlertMsg('Email does not exist');
        }
        else if(data.error === 'login-type-google'){
          setAlertMsg('Please sign-in using Google.');
        }
        else{

          Swal.fire({
              icon: 'success',
              title: 'Email Sent!',
              text: 'Instructions has been sent to your email.',
              showConfirmButton: false,
              timer: 3000
          });

        }

      });

    }

  }

	return (
    <React.Fragment>
      <form className='forgot__form'id="login-in">
        <h1 className="login__title">Forgot Password</h1>
        {alertMsg
          ?
            <Alert className='mx-auto' variant='danger'>{alertMsg}</Alert>
          :
            ''
        }
        <div className="login__box">
          <i className='bx bx-at login__icon'></i>
          <input 
            type="text" 
            placeholder="Email" 
            className="login__input"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>

        <a className="login__button" onClick={() => authenticate()}>Submit</a>

        <div>
          <span 
            className="login__signin" 
            id="sign-up" 
            onClick={() => getForm('login')}
          > 
            Back
          </span>
        </div>
        
      </form>
    </React.Fragment>
	)
}