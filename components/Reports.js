import { useEffect, useState, useContext } from 'react';
import { MDBDataTableV5 } from 'mdbreact';
import moment from 'moment';
import { numberWithCommas } from '../helpers';
import Swal from 'sweetalert2';

export default function Reports(){

	const [datatable, setDatatable] = useState({});
	useEffect(() => {
		Swal.fire({
	        title: 'Retrieving transactions...',
	        allowOutsideClick: false,
	        didOpen: () => {
	          Swal.showLoading()
	        }
	    });
		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/details`, {
		    "method": "GET",
		    "headers": {
		      "Authorization": `Bearer ${localStorage.getItem('token')}`
		    }
		})
		.then(res => res.json())
		.then(data => {
			const tableData = data.transactions.reverse().map(transaction => {
				return {
			        title: transaction.title,
			        type: transaction.transactionType,
			        category: transaction.category,
			        amount: `\u20B1 ${numberWithCommas(transaction.amount)}`,
			        date: moment(transaction.date).format("MM/DD/YYY, hh:mm:ss a")
			    }
			});

			setDatatable({
				columns: [
			      {
			        label: 'Title',
			        field: 'title'
			      },
			      {
			        label: 'Type',
			        field: 'type'
			      },
			      {
			        label: 'Category',
			        field: 'category'
			      },
			      {
			        label: 'Amount',
			        field: 'amount'
			      },
			      {
			        label: 'Date',
			        field: 'date',
			        sort: 'desc'
			      }
			    ],
			    rows: tableData
			});

			Swal.close();
		});
	}, []);


	return(
		<MDBDataTableV5 
			hover 
			striped 
			entriesOptions={[10, 15, 20]} 
			entries={10} 
			pagesAmount={4}
			pagingTop
			searchTop
      		searchBottom={false}
			data={datatable}
		/>
	)
}