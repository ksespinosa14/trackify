import { useEffect, useState } from 'react';
import { Card, Row, Col, Alert } from 'react-bootstrap';
import { Doughnut } from 'react-chartjs-2';
import { colorRandomizer } from '../helpers';
import Loading from './Loading';

export default function IncomePie(){

	const [categories, setCategories] = useState([]);
	const [amount, setAmount] = useState([]);
	const [bgColors, setBgColors] = useState([]);

	const [loader, setLoader] = useState(true);

	useEffect(() => {
		setLoader(true);
		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/details`, {
		    "method": "GET",
		    "headers": {
		      "Authorization": `Bearer ${localStorage.getItem('token')}`
		    }
		})
		.then(res => res.json())
		.then(data => {
			const income = data.transactions.reduce((a,b) => {
				if(b.transactionType === 'Income'){

					let o = a.filter(function(obj){
				        return obj.category==b.category;
				    }).pop() || {category:b.category, amount:0};

				    o.amount += b.amount;
				    a.push(o);

				}
				return a;
			}, [])
			.filter(function(itm, i, a) {
                return i == a.indexOf(itm);
            });

			setCategories(income.map(data => data.category));	
			setAmount(income.map(data => data.amount));
			setLoader(false);
		});
	}, []);

	useEffect(() => {

		if(amount.length > 0){

			setBgColors(amount.map(() => `#${colorRandomizer()}`));
		}

	}, [amount, categories]);


	return (
		<Card className='my-2'>
			<Card.Header className='bg-white'>

				<Row className='d-flex align-items-center my-3 px-3'>
					<h4>Income Categories</h4>
				</Row>

		  	</Card.Header>
		  	<Card.Body>

		  		{loader
					?
						<Loading />
					:
						''
				}

				{amount.length === 0 
		  			?
		  				loader === false
							?
								<Alert variant='info'>No transactions found.</Alert>
							:
								''
		  			:
				  		<Doughnut data={{
							datasets: [{
								data: amount,
								backgroundColor: bgColors
							}],
							labels: categories

						}} redraw={false} />

		  		}
		  	</Card.Body>
		</Card>
	)
}