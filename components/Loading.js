import { Container, Spinner, Jumbotron } from 'react-bootstrap';
export default function Loading(){
	return (
		<Container className='vertical-center'>
			<div className="mx-auto">
				<Spinner variant="primary" animation="border" role="status" >
				  <span className="sr-only">Loading...</span>
				</Spinner>
			</div>
		</Container>
	)
}