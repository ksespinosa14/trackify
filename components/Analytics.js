import React, { useEffect, useState } from 'react';
import { Jumbotron, Row, Col } from 'react-bootstrap';
import { Line, Pie } from 'react-chartjs-2';
import Trends from './Trends';
import TotalPie from './TotalPie';
import IncomePie from './IncomePie';
import ExpensesPie from './ExpensesPie';

export default function Analytics(){

	return(
		<React.Fragment>
			<Jumbotron>

				<Row>
					<Col md={12} lg={6}>
						<Trends />
					</Col>

					<Col md={12} lg={6}>
						<TotalPie />
					</Col>

				</Row>

				<Row>
					<Col md={12} lg={6}>
						<IncomePie />
					</Col>

					<Col md={12} lg={6}>
						<ExpensesPie />
					</Col>

				</Row>

			</Jumbotron>
		</React.Fragment>
	)

}