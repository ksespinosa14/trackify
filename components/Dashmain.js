import React, { useState, useEffect, useContext } from 'react';
import { Row, Col, Jumbotron } from 'react-bootstrap';
import UserContext from '../UserContext';
import TransactionForm from './TransactionForm';
import CategoryForm from './CategoryForm';
import LatestTransactions from './LatestTransactions';

export default function Dashmain(){
	const { user } = useContext(UserContext);

	return (
		<React.Fragment>
			<Row>
				<Col xs={12} md={6}>
					<CategoryForm />
					<TransactionForm />
				</Col>

				<Col xs={12} md={6}>
					<LatestTransactions />
				</Col>

			</Row>
		</React.Fragment>
	)
}