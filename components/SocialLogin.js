import { GoogleLogin } from 'react-google-login';

export default function SocialLogin({ authenticateGoogleToken }){
	return(
		<div className="login__social">
          <GoogleLogin
          	className="rounded mb-0"
            clientId="664598190717-tke5be2abdqoaogklsbbm5tlghve20td.apps.googleusercontent.com"
            theme="dark"
            onSuccess={authenticateGoogleToken}
            onFailure={authenticateGoogleToken}
            cookiePolicy={'single_host_origin'}
          />
        </div>
	)
}