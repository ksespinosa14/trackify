import React, { useState, useContext } from 'react';
import { useRouter } from 'next/router';
import { Alert } from 'react-bootstrap';
import UserContext from '../UserContext';
import SocialLogin from './SocialLogin';
import Swal from 'sweetalert2';

export default function LoginForm({ getForm, authenticateGoogleToken, retrieveUserDetails }){

  const { user, setUser } = useContext(UserContext);
  const Router = useRouter();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [alertMsg, setAlertMsg] = useState('');

  function authenticate(){
    if(email === '' || password === '') setAlertMsg('Please fill-out all fields.');
    else{
      setAlertMsg('');
      Swal.fire({
        title: 'Please wait...',
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading()
        }
      });
      fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            // Successful authentication will return a JWT via the response accessToken property
            if(data.accessToken){

                // Store JWT in local storage
                localStorage.setItem('token', data.accessToken);
                retrieveUserDetails(data.accessToken);

            }
            else{ // Authentication failure
              Swal.close();
              if (data.error === 'does-not-exist') {
                setAlertMsg('Invalid Username');
              } 
              else if (data.error === 'login-type-error') {
                setAlertMsg('You may have registered through a different login procedure, try an alternative login procedure.');
              } 
              else if (data.error === 'unverified-email') {
                setAlertMsg('Email not verified. Please login to your email and verify.');
              } 
              else if(data.error === 'incorrect-password') {
                setAlertMsg('Invalid Password');
              }
            }
        });
    }
  }

	return (
    <React.Fragment>
      <form className='login__register'id="login-in">
        <h1 className="login__title">Sign In</h1>
        {alertMsg
          ?
            <Alert className='mx-auto' variant='danger'>{alertMsg}</Alert>
          :
            ''
        }
        <div className="login__box">
          <i className='bx bx-at login__icon'></i>
          <input 
            type="text" 
            placeholder="Email" 
            className="login__input"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>

        <div className="login__box">
          <i className='bx bx-lock login__icon'></i>
          <input 
            type="password" 
            placeholder="Password" 
            className="login__input"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>

        <a className="login__forgot" onClick={() => getForm('forgot')}>Forgot password?</a>

        <a className="login__button" onClick={() => authenticate()}>Sign In</a>

        <div>
          <span className="login__account">Don't have an Account? </span>
          <span 
            className="login__signin" 
            id="sign-up" 
            onClick={() => getForm('registration')}
          > 
            Sign Up
          </span>
        </div>

        <SocialLogin authenticateGoogleToken={authenticateGoogleToken} />
        
      </form>
    </React.Fragment>
	)
}