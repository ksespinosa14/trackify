import { useEffect, useState } from 'react';
import { Card, Form, Row, Col, Alert } from 'react-bootstrap';
import { Line } from 'react-chartjs-2';
import Loading from './Loading';
import moment from 'moment';

export default function Trends({ setTrend }){

	const [total, setTotal] = useState(0);
	const [totalIncome, setTotalIncome] = useState(0);
	const [totalExpenses, setTotalExpenses] = useState(0);
	const [trends, setTrends] = useState([]);

	const [dateFrom, setDateFrom] = useState('2021-01-01');
	const [dateTo, setDateTo] = useState('2021-12-31');

	const [loader, setLoader] = useState(true);

	useEffect(() => {
		setLoader(true);
		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/details`, {
		    "method": "GET",
		    "headers": {
		      "Authorization": `Bearer ${localStorage.getItem('token')}`
		    }
		})
		.then(res => res.json())
		.then(data => {

			const transactions = data.transactions.filter(transaction => {
				const creationDate = moment(transaction.date).format('MM/DD/YYYY');
				const dateFromFilter = moment(dateFrom).format('MM/DD/YYYY');
				const dateToFilter = moment(dateTo).format('MM/DD/YYYY');
				return (creationDate >= dateFromFilter && creationDate <= dateToFilter);

			});

			const income = transactions.reduce((a, b) => {
				return a + (b.transactionType === 'Income' ? b.amount : 0);
			}, 0);
			setTotalIncome(income);

			const expenses = transactions.reduce((a, b) => {
				return a + (b.transactionType === 'Expense' ? b.amount : 0);
			}, 0);
			setTotalExpenses(expenses);

			const total = transactions.reduce((a, b) => {
				return a + (b.transactionType === 'Expense' ? (-b.amount) : b.amount);
			}, 0);
			setTotal(total);

			const trendData = [];
			transactions.forEach((transaction, index) => {
				
				if(transaction.transactionType === 'Expense'){
					if(index === 0) trendData.push(-Math.abs(transaction.amount))
					else{
						trendData.push(trendData[index-1] + -Math.abs(transaction.amount))
					}
				}
				else{
					if(index === 0) trendData.push(Math.abs(transaction.amount))
					else{
						trendData.push(trendData[index-1] + Math.abs(transaction.amount))
					}
				}
			}); 
			setTrends(trendData);
			setLoader(false);

		});
	}, [dateFrom, dateTo]);

	return (

		<Card className='my-2'>

			<Card.Header className='bg-white'>

				<Row className='d-flex align-items-center'>

					<Col lg="6" md="12">
						<h4>Balance Trends</h4>
					
					</Col>

					<Col lg="6" md="12">


						<Form>

						    <Form.Row>

						    	<Form.Group as={Col} lg="6" md="12" controlId="dateFrom">
							        <Form.Label>From</Form.Label>
							        <Form.Control
							            type="date"
							            value={dateFrom}
							            onChange={(e) => setDateFrom(e.target.value)}
							        />
						        </Form.Group>
						   
						        <Form.Group as={Col} lg="6" md="12" controlId="dateTo">
							        <Form.Label>To</Form.Label>
							        <Form.Control
							            type="date"
							            value={dateTo}
							            onChange={(e) => setDateTo(e.target.value)}
							        />
						        </Form.Group>


						    </Form.Row>

						</Form>


					</Col>

				</Row>


			</Card.Header>

			<Card.Body>
				{loader
					?
						<Loading />
					:
						''
				}
				
				{trends.length === 0
					?
						loader === false
							?
								<Alert variant='info'>No transactions found.</Alert>
							:
								''
					:
						loader === false
							?
								<Line data={
									{
										labels: trends,
									  	datasets: [
										    {
										      label: '',
										      data: trends,
										      fill: false,
										      borderColor: "rgba(75,192,192,1)"
										    }
									  	]
									}	
								} />
							:
								''
				}
				


			</Card.Body>

		</Card>
	)
}
