import { useState, useEffect } from 'react';
import { Jumbotron, Container, Card, Row, Col, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function User(){

	const [email, setEmail] = useState('');
	const [userName, setUsername] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [loginType, setLoginType] = useState('');

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/details`, {
		    "method": "GET",
		    "headers": {
		      "Authorization": `Bearer ${localStorage.getItem('token')}`
		    }
		})
		.then(res => res.json())
		.then(data => {
			
			setEmail(data.email);
			setUsername(data.userName || '');
			setFirstName(data.firstName || '');
			setLastName(data.lastName || '');
			setMobileNo(data.mobileNo || '');
			setLoginType(data.loginType || '');

		});
	}, []);

	function updateUserInfo(e){
		e.preventDefault();
		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/`, {
		    "method": "PUT",
		    "headers": {
		      "Authorization": `Bearer ${localStorage.getItem('token')}`,
		      "Content-Type": 'application/json'
		    },
		    body: JSON.stringify({
		    	userName: userName,
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobileNo
		    })
		})
		.then(res => res.json())
		.then(data => {

			if(data){
				Swal.fire({
				  position: 'top-end',
				  icon: 'success',
				  title: 'Update Successful.',
				  showConfirmButton: false,
				  timer: 1500
				});
			}
			else{
				Swal.fire({
				  position: 'top-end',
				  icon: 'error',
				  title: 'Something went wrong please try again later.',
				  showConfirmButton: false,
				  timer: 1500
				});
			}

		});
	}

	function updatePassword(e){
		e.preventDefault();

		if(password1 === '' || password2 === '' || (password1 !== password2)){
			Swal.fire({
				  position: 'top-end',
				  icon: 'error',
				  title: 'Password Update Error.',
				  text: "Please make sure password isn't blank and is matching",
				  showConfirmButton: false,
				  timer: 1500
				});
			return false;
		}

		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/password`, {
		    "method": "PUT",
		    "headers": {
		      "Authorization": `Bearer ${localStorage.getItem('token')}`,
		      "Content-Type": 'application/json'
		    },
		    body: JSON.stringify({
		    	password: password1
		    })
		})
		.then(res => res.json())
		.then(data => {

			setPassword1('');
			setPassword2('');

			if(data){
				Swal.fire({
				  position: 'top-end',
				  icon: 'success',
				  title: 'Password Updated!.',
				  showConfirmButton: false,
				  timer: 1500
				});
			}
			else{
				Swal.fire({
				  position: 'top-end',
				  icon: 'error',
				  title: 'Something went wrong please try again later.',
				  showConfirmButton: false,
				  timer: 1500
				});
			}

		});
	}

	return(
		<Jumbotron>
			<Container>

				<Card className='p-5'>
					<Row className="align-items-center">
						<Col className="text-center">
							<i className='bx bxs-user-circle profile__icon'></i>
						</Col>
					</Row>
					<Row>
						<Col>
							<Form className='my-3' onSubmit={(e) => updateUserInfo(e)}>
								<h4>User Information:</h4>
								<Form.Row>

							    	<Form.Group as={Col} md="6" sm="12" controlId="email">
								        <Form.Label>Email:</Form.Label>
								        <Form.Control
								            type="text"
								            value={email}
								            disabled
								        />
							        </Form.Group>

							    </Form.Row>


							    <Form.Row>

							    	<Form.Group as={Col} md="6" sm="12" controlId="username">
								        <Form.Label>Username:</Form.Label>
								        <Form.Control
								            type="text"
								            value={userName}
								            onChange={(e) => setUsername(e.target.value)}
								        />
							        </Form.Group>

							    	<Form.Group as={Col} md="6" sm="12" controlId="mobileNo">
								        <Form.Label>Mobile Number:</Form.Label>
								        <Form.Control
								            type="text"
								            value={mobileNo || ''}
								            onChange={(e) => setMobileNo(e.target.value)}
								        />
							        </Form.Group>
							   

							    </Form.Row>

							    <Form.Row>

							    	<Form.Group as={Col} md="6" sm="12" controlId="firstName">
								        <Form.Label>First Name:</Form.Label>
								        <Form.Control
								            type="text"
								            value={firstName}
								            onChange={(e) => setFirstName(e.target.value)}
								        />
							        </Form.Group>
							   
							        <Form.Group as={Col} md="6" sm="12" controlId="lastName">
								        <Form.Label>Last Name:</Form.Label>
								        <Form.Control
								            type="text"
								            value={lastName}
								            onChange={(e) => setLastName(e.target.value)}
								        />
							        </Form.Group>


							    </Form.Row>

							    <Button type='submit' className='float-right'>Update</Button>

							</Form>

							{loginType === 'email'
								?
									<Form className='my-3' onSubmit={(e) => updatePassword(e)}>
										<h4>Update Password:</h4>
										<Form.Row>

									    	<Form.Group as={Col} md="6" sm="12" controlId="password1">
										        <Form.Label>Password:</Form.Label>
										        <Form.Control
										            type="password"
										            value={password1}
										            onChange={(e) => setPassword1(e.target.value)}
										        />
									        </Form.Group>
									   
									        <Form.Group as={Col} md="6" sm="12" controlId="password2">
										        <Form.Label>Verify Password:</Form.Label>
										        <Form.Control
										            type="password"
										            value={password2}
										            onChange={(e) => setPassword2(e.target.value)}
										        />
									        </Form.Group>

									    </Form.Row>
									    <Button type='submit' className='float-right'>Update</Button>
									</Form>
								:
									''
							}

							

						</Col>
						
					</Row>
				</Card>
			</Container>
		</Jumbotron>
	)
}