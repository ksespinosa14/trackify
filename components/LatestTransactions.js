import React, { useEffect, useState, useContext } from 'react';
import { Jumbotron, Card, Alert} from 'react-bootstrap'
import moment from 'moment';
import { numberWithCommas } from '../helpers';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function LatestTransactions(){

	const [transactions, setTransactions] = useState([]);
	const { success } = useContext(UserContext);

	useEffect(() => {
		Swal.fire({
		    title: 'Please wait...',
		    allowOutsideClick: false,
		    didOpen: () => {
		      Swal.showLoading()
		    }
	  	});
		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/details`, {
		    "method": "GET",
		    "headers": {
		      "Authorization": `Bearer ${localStorage.getItem('token')}`
		    }
		})
		.then(res => res.json())
		.then(data => {
			if(data.transactions.length > 0) {
				setTransactions(data.transactions.reverse().slice(0, 5).map(transaction => {

					return (
						<Card key={transaction._id} className={transaction.transactionType === 'Income' ? 'my-3 border-success' : 'my-3 border-danger'}>
							<Card.Body>
								<Card.Title 
									className={transaction.transactionType === 'Income' ? 'text-success' : 'text-danger'}
								>
									<small className='float-right'>
										{transaction.transactionType === 'Income' ? <span>&#43; </span> : <span>&#8722; </span>}
										&#8369;{numberWithCommas(transaction.amount)}
									</small>
									<h5>{transaction.title}</h5>
								</Card.Title>
								<Card.Text><small className="text-muted">{moment(transaction.date).format('MM/DD/YYYY, h:mm:ss a')}</small></Card.Text>
							</Card.Body>
						</Card>
					)
				}));
			}
			Swal.close();
		});
	}, [success]);

	return(
		<React.Fragment>
			<Jumbotron>
				<h4>Latest Transactions</h4>
				<hr className="my-4" />
				{transactions.length > 0
					?
						transactions
					:
						<Alert variant='warning'>No transactions yet.</Alert>
				}
			</Jumbotron>
		</React.Fragment>
	)
}