import React from 'react';
import { useState, useContext, useEffect } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { GoogleLogin } from 'react-google-login';
import styles from '../styles/Home.module.css';
import LoginForm from '../components/LoginForm';
import RegistrationForm from '../components/RegistrationForm';
import ForgotForm from '../components/ForgotForm';
import Loading from '../components/Loading';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Home() {
  const { user, setUser } = useContext(UserContext);

  const Router = useRouter();

  // if already logged in redirect to dashboard
  useEffect(() => {
    if (user.id !== null) {
      Router.push('/dashboard');
    }
  }, [user]);

  const [form, setForm] = useState('login');

  function getForm(formName) {
    setForm(formName);
  }

  function retrieveUserDetails(accessToken) {
    // Send a fetch request to decode JWT and obtain user ID and role for storing in context
    fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // Set the global user state to have properties containing authenticated user's ID
        setUser({
          id: data._id,
        });
        Swal.close();
        Router.push('/dashboard');
      });
  }

  const authenticateGoogleToken = (response) => {
    Swal.fire({
      title: 'Please wait...',
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      },
    });

    fetch(
      `${process.env.NEXT_PUBLIC_API_BASE_URL}/users/verify-google-id-token`,
      {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ tokenId: response.tokenId }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (typeof data.accessToken !== 'undefined') {
          localStorage.setItem('token', data.accessToken);
          retrieveUserDetails(data.accessToken);
        } else {
          Swal.close();
          if (data.error === 'google-auth-error') {
            Swal.fire({
              icon: 'error',
              title: 'Google Auth Error',
              showConfirmButton: false,
              timer: 1500,
            });
          } else if (data.error === 'login-type-error') {
            Swal.fire({
              icon: 'error',
              title: 'Login Type Error',
              showConfirmButton: false,
              timer: 1500,
            });
          }
        }
      });
  };

  return (
    <React.Fragment>
      <Head>
        <title>Trackify</title>
        <link rel='shortcut icon' href='/logo-removebg-preview.png' />
      </Head>
      {user.id !== null ? (
        ''
      ) : (
        <div className='login'>
          <div className='login__content'>
            <div className='login__img'>
              <img
                className='login__img-img'
                src='/logo-removebg-preview.png'
              />
            </div>

            <div className='login__forms'>
              {form === 'login' ? (
                <LoginForm
                  getForm={getForm}
                  authenticateGoogleToken={authenticateGoogleToken}
                  retrieveUserDetails={retrieveUserDetails}
                />
              ) : (
                ''
              )}
              {form === 'registration' ? (
                <RegistrationForm
                  getForm={getForm}
                  authenticateGoogleToken={authenticateGoogleToken}
                />
              ) : (
                ''
              )}
              {form === 'forgot' ? <ForgotForm getForm={getForm} /> : ''}
            </div>
          </div>
        </div>
      )}
    </React.Fragment>
  );
}
