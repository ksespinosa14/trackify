import React, { useEffect } from 'react';
import { useRouter } from 'next/router'
import Head from 'next/head';
import Loading from '../components/Loading';
import Swal from 'sweetalert2';

export default function Confirmation({params}){
	const Router = useRouter();

	useEffect(() => {
		if(Router.query.token !== undefined){
			fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/confirmation/${Router.query.token}`,{
				method: 'PUT',
				header: {
					'Content-Type': 'application/json'
				}
			})
			.then(res => res.json())
			.then(data => {
				if(data){
					Swal.fire({
					  	icon: 'success',
					  	title: 'Email successfully verified!',
					  	showConfirmButton: false,
						timer: 1500
					});
				}
				else{
					Swal.fire({
					  	icon: 'error',
					  	title: 'Something went wrong!',
					  	showConfirmButton: false,
						timer: 1500
					});
				}
				Router.push('/');
			});
		}
	}, [Router]);

	return (
		<React.Fragment>
			<Head>
		        <title>Trackify</title>
		        <link rel="shortcut icon" href="/logo-removebg-preview.png" />
		    </Head>
			<Loading />
		</React.Fragment>
	)
}

// export function getServerSideProps(context){
// 	return {
// 	    props: {params: context.params}
// 	  };
// }