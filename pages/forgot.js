import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router'
import Head from 'next/head';
import Loading from '../components/Loading';
import Swal from 'sweetalert2';

export default function Forgot({params}){
	const Router = useRouter();
	const [token, setToken] = useState('');

	useEffect(() => {
		if(Router.query.token !== undefined){

			setToken(Router.query.token);

			Swal.fire({
			  title: 'Set New Password',
			  html: `<input type="password" id="password1" class="swal2-input" placeholder="Password">
			  <input type="password" id="password2" class="swal2-input" placeholder="Confirm Password">`,
			  confirmButtonText: 'Confirm',
			  focusConfirm: false,
			  preConfirm: () => {
			    const pass1 = Swal.getPopup().querySelector('#password1').value
			    const pass2 = Swal.getPopup().querySelector('#password2').value
			    if (!pass1 || !pass2) {
			    	Swal.showValidationMessage(`Please fill-out all fields.`)
			    }
			    else if(pass1 !== pass2){
			    	Swal.showValidationMessage(`Password doesn't match`)
			    }
			    else{
			    	return { password: pass1 }
			    }
			  }
			})
			.then((result) => {

			  const newPassword = result.value.password;

			  fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/reset`, {
			  	method: 'PUT',
			  	headers: {
			  		'Content-Type': 'application/json'
			  	},
			  	body: JSON.stringify({
			  		password: newPassword,
					resetId: Router.query.token
			  	})
			  })
			  .then(res => res.json())
			  .then(data => {
			  	console.log(data);
			  	if(data.error === 'expired-token'){
			  		Swal.fire({
			            icon: 'error',
			            title: 'Expired Request',
			            showConfirmButton: false,
			            timer: 1500
			        });
			  	}
			  	else if(data.error === 'invalid-token'){
			  		Swal.fire({
			            icon: 'error',
			            title: 'Invalid Request',
			            showConfirmButton: false,
			            timer: 1500
			        });
			  	}
			  	else{
			  		Swal.fire({
			            icon: 'success',
			            title: 'Success',
			            showConfirmButton: false,
			            timer: 1500
			        });
			  	}
			  	setTimeout(() => {
			  		Router.push('/');
			  	}, 1500);

			  })

			})
		}
	}, [Router]);

	return (
		<React.Fragment>
			<Head>
		        <title>Trackify</title>
		        <link rel="shortcut icon" href="/logo-removebg-preview.png" />
		    </Head>
		    {token === ''
		    	?
		    		<Loading />
		    	:
		    		''
		    }
			
		</React.Fragment>
	)
}

