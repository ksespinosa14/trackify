import React, { useState, useEffect, useContext } from 'react';
import Head from 'next/head'
import Link from "next/link";
import { useRouter } from 'next/router';
import UserContext from '../UserContext';
import Dashmain from '../components/Dashmain';
import Reports from '../components/Reports';
import Analytics from '../components/Analytics';
import User from '../components/User';
import moment from 'moment';

export default function Dashboard(){
	const { user, unsetUser } = useContext(UserContext);
	const { success } = useContext(UserContext);

	const [toggle, setToggle] = useState('');
	const [total, setTotal] = useState(0);
	const [totalIncome, setTotalIncome] = useState(0);
	const [totalExpenses, setTotalExpenses] = useState(0);
	const Router = useRouter();
    
	// if not logged in redirect to homepage
	useEffect(() => {
		if(localStorage.getItem("token") === null) Router.push('/');
	}, [user]);

	//logout hook
	useEffect(() => {
		if(Router.query.view === 'Logout'){
			unsetUser();
        	Router.push('/');
		}
	}, [Router]);

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/details`, {
		    "method": "GET",
		    "headers": {
		      "Authorization": `Bearer ${localStorage.getItem('token')}`
		    }
		})
		.then(res => res.json())
		.then(data => {

			const income = data.transactions.reduce((a, b) => {
				return a + (b.transactionType === 'Income' ? b.amount : 0);
			}, 0);
			setTotalIncome(income);

			const expenses = data.transactions.reduce((a, b) => {
				return a + (b.transactionType === 'Expense' ? b.amount : 0);
			}, 0);
			setTotalExpenses(expenses);

			const total = data.transactions.reduce((a, b) => {
				return a + (b.transactionType === 'Expense' ? (-b.amount) : b.amount);
			}, 0);
			setTotal(total);


		});
	}, [success]);

	function showNavbar(){
		if(toggle === '') setToggle('toggled');
		else setToggle('');
	}

	return (
		<React.Fragment>
			<Head>
	        	<title>Trackify</title>
	        	<link rel="shortcut icon" href="/logo-removebg-preview.png" />
	      	</Head>
	      	{user.id !== null 
				? 
					<main id="body-pd" className={toggle === '' ? '' : 'body-pd'}>
				        <header className={toggle === '' ? 'header' : 'header body-pd'} id="header">
				            <div className="header__toggle">
				                <i className={toggle === '' ? 'bx bx-menu' : 'bx bx-x'} id="header-toggle" onClick={() => showNavbar()}></i>
				                <div className="ml-2 d-none d-sm-inline">
				                	{(Router.query.view) === undefined ? 'Dashboard' :  Router.query.view}
				                </div>
				            </div>

				            <div className="d-flex justify-content-between">

					    		<div className='mx-3'>
									<small className='font-weight-bold'>Income: </small>
									<small className='text-success'>&#8369; {totalIncome}</small>
								</div>

								<div className='mx-3'>
									<small className='font-weight-bold'>Expense: </small>
									<small className='text-danger'>&#8369; {totalExpenses}</small>
								</div>

								<div className='mx-3'>
									<small className='font-weight-bold'>Status: </small>
									<small className={total > 0 ? 'text-success' : 'text-danger'}>
										{total > 0 ? <span>&#43; </span> : <span>&#8722; </span>} 
										{total}
									</small>
								</div>

					    	</div>

				        </header>

				        <div className={toggle === '' ? 'l-navbar' : 'l-navbar show'} id="nav-bar">
				            <nav className="nav">
				                <div>
				                    <a href="#" className="nav__logo">
				                        <i className='bx bxs-pie-chart-alt-2 nav__logo-icon'></i>
				                        <span className="nav__logo-name">Trackify</span>
				                    </a>
				                    <div className="nav__list">

				                        <Link href="/dashboard">
					                    	<a className={Router.query.view === undefined ?  'nav__link active' : 'nav__link'}>
					                            <i className='bx bx-grid-alt nav__icon'></i>
				                            	<span className="nav__name">Dashboard</span>
					                        </a>
				                    	</Link>

				                        <Link href="/dashboard?view=User">
					                    	<a className={Router.query.view === 'User' ?  'nav__link active' : 'nav__link'}>
					                            <i className='bx bx-user nav__icon'></i>
					                            <span className="nav__name">User</span>
					                        </a>
				                    	</Link>

				                    	<Link href="/dashboard?view=Reports">
					                    	<a className={Router.query.view === 'Reports' ?  'nav__link active' : 'nav__link'}>
					                            <i className='bx bx-message-square-detail nav__icon'></i>
				                            	<span className="nav__name">Reports</span>
					                        </a>
				                    	</Link>

				                    	<Link href="/dashboard?view=Analytics">
					                    	<a className={Router.query.view === 'Analytics' ?  'nav__link active' : 'nav__link'}>
					                            <i className='bx bx-bar-chart-alt-2 nav__icon'></i>
				                            	<span className="nav__name">Analytics</span>
					                        </a>
				                    	</Link>

				                    </div>
				                </div>

				                <Link href="/dashboard?view=Logout">
			                    	 <a href="#" className={Router.query.view === 'Logout' ?  'nav__link active' : 'nav__link'}>
					                    <i className='bx bx-log-out nav__icon'></i>
					                    <span className="nav__name">Logout</span>
					                </a>
		                    	</Link>
				               
				            </nav>
				        </div>

				        {
				        	(Router.query.view === undefined) ? <Dashmain /> : ''
				        }
				        {
				        	(Router.query.view === 'User') ? <User /> : ''
				        }

				        {
				        	(Router.query.view === 'Reports') ? <Reports /> : ''
				        }
				        {
				        	(Router.query.view === 'Analytics') ? <Analytics /> : ''
				        }

				    </main>
				:
					''
			}
			
		</React.Fragment>
		
	)
}